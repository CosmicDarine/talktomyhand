# Talk To My Hand

[![pipeline status](https://gitlab.com/CosmicDarine/talktomyhand/badges/master/pipeline.svg)](https://gitlab.com/CosmicDarine/talktomyhand/-/commits/master)

## Projet : Application pour apprendre les langues des signes

### But
Ce projet est destiné à populariser l'apprentissage des langues des signes. 

## Site web
https://talktomyhand.ch
## Repositories
Le projet a été scindé en deux catégories distinctes, pour une meilleure organisation. 
### Front-end
https://gitlab.com/CosmicDarine/talktomyhand-frontend

### Back-end
https://gitlab.com/CosmicDarine/talktomyhand-backend
